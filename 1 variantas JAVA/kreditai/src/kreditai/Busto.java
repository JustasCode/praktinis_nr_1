package kreditai;

import java.util.Scanner;

public class Busto {

	float m;
	float b;
	float t,t1;
	float v;
	
	Scanner in = new Scanner(System.in);
	
	void busto(){
		System.out.println("Iveskite vaiku skaiciu nuo 0-10");
		int x = in.nextInt();
		System.out.println("Seimos pajamos nuo 0 iki 30000eur");
		int y = in.nextInt();
		System.out.println("Seimos finansiniai isipareigojimai nuo 0 iki 15000eur"); 
		int z = in.nextInt();
		System.out.println("Paskola neribojama");
		int c = in.nextInt();
		System.out.println("Laikotarpis 50 metų - pastato amžius, bet ne daugiau 40 metų");
		int l = in.nextInt();
		System.out.println("--------------------------------------------------------------");
		System.out.println("                      Kredito skaiciuokle                     ");
		System.out.println("--------------------------------------------------------------");
		System.out.println("Menesine imoka : ");
		m = men_imoka(c,l);
		System.out.println(m);
		System.out.println("Bendra grazintine suma : ");
		b = (c+(c*13f/100f));
		System.out.println(b);
		System.out.println("--------------------------------------------------------------");
		System.out.println("             Sutarties sudarimo apskaiciavimai                ");
		System.out.println("--------------------------------------------------------------"); 
		t=(200*x);
		t1=(m+(m*40f/100f));

		if(y<=t){
			System.out.println("Kreditas nesuteikiamas");  
		}
		else if(t1>((y-t)-z)){
				System.out.println("Kreditas nesuteikiamas");
		}     
		else{
				System.out.println("Kreditas suteikiamas"); 
		}
		
		System.out.println("--------------------------------------------------------------");
		System.out.println("               Kredito grazinimo laikotarpis                  ");
		System.out.println("--------------------------------------------------------------"); 
		v=b/m;
		System.out.println("Kredito grazinimas menesiais");
		System.out.println(v); 
		System.out.println("Kredito grazinimas metais");
		System.out.println(v/12);
		System.out.println("                                                              ");
	}
	
	public float men_imoka(int c, int l){

		return c / (l*12);
		
	}
	
}